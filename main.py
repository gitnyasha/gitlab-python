import http.client
import csv
import json
from os import environ
from dotenv import load_dotenv

load_dotenv()


def get_data():
    conn = http.client.HTTPSConnection("gitlab.com")

    headersList = {
        "PRIVATE-TOKEN": environ.get('ACCESS_TOKEN')
    }

    payload = ""

    conn.request("GET", "/api/v4/projects/41070985/repository/commits",
                 payload, headersList)
    response = conn.getresponse()
    result = response.read()

    return json.loads(result.decode("utf-8"))


def generate_csv(data):
    with open('gitlab-python.csv', 'w', newline='') as csvfile:
        fieldnames = ['id', 'short_id', 'created_at', 'parent_ids', 'title', 'message', 'author_name', 'author_email',
                      'authored_date', 'committer_name', 'committer_email', 'committed_date', 'trailers', 'web_url']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
        writer.writeheader()
        for i in data:
            writer.writerow(i)


generate_csv(get_data())
